Source: python-shellescape
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-shellescape
Vcs-Git: https://salsa.debian.org/python-team/packages/python-shellescape.git
Homepage: https://github.com/chrissimpkins/shellescape

Package: python3-shellescape
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Description: escapes characters to safely assemble command lines
 This python3 library helps to escape a string to safely use it as a token in
 a shell command.
 .
 The shellescape Python module defines the shellescape.quote() function
 that returns a shell-escaped version of a Python string. This is a
 backport of the shlex.quote() function from Python 3.4.3 that makes
 it accessible to users of Python 3 versions < 3.3 and all Python 2.x
 versions.
